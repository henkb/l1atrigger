#ifndef TTCVIMODULE_H
#define TTCVIMODULE_H

#include "TtcGenerator.h"

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

#define TTC_CSR1_TRIGSELECT_MASK    0x0007
#define TTC_CSR1_TRIGSELECT_L1A0    0
#define TTC_CSR1_TRIGSELECT_L1A1    1
#define TTC_CSR1_TRIGSELECT_L1A2    2
#define TTC_CSR1_TRIGSELECT_L1A3    3
#define TTC_CSR1_TRIGSELECT_VME     4
#define TTC_CSR1_TRIGSELECT_RANDOM  5
#define TTC_CSR1_TRIGSELECT_CALIB   6
#define TTC_CSR1_TRIGSELECT_DISABLE 7
#define TTC_CSR1_INTERNAL_ORBIT     0x0008
#define TTC_CSR1_L1A_FIFO_FULL      0x0010
#define TTC_CSR1_L1A_FIFO_EMPTY     0x0020
#define TTC_CSR1_L1A_FIFO_RESET     0x0040
#define TTC_CSR1_VME_PENDING        0x0080
#define TTC_CSR1_BCDELAY_MASK       0x0F00
#define TTC_CSR1_BCDELAY_SHIFT      8
#define TTC_CSR1_ORBIT_COUNT        0x8000
#define TTC_CSR1_RANDOMRATE_MASK    0x7000
#define TTC_CSR1_RANDOMRATE_SHIFT   12
#define TTC_CSR1_RANDOMRATE_1       0
#define TTC_CSR1_RANDOMRATE_100     1
#define TTC_CSR1_RANDOMRATE_1K      2
#define TTC_CSR1_RANDOMRATE_5K      3
#define TTC_CSR1_RANDOMRATE_10K     4
#define TTC_CSR1_RANDOMRATE_25K     5
#define TTC_CSR1_RANDOMRATE_50K     6
#define TTC_CSR1_RANDOMRATE_100K    7

#define TTC_CSR2_BGO0_RESET         0x1000
#define TTC_CSR2_BGO1_RESET         0x2000
#define TTC_CSR2_BGO2_RESET         0x4000
#define TTC_CSR2_BGO3_RESET         0x8000
#define TTC_CSR2_BGO_RESET          (TTC_CSR2_BGO0_RESET|TTC_CSR2_BGO1_RESET|\
                                     TTC_CSR2_BGO2_RESET|TTC_CSR2_BGO3_RESET)

/* ------------------------------------------------------------------------- */

typedef struct broadcast
{
  unsigned short mode;
  unsigned short delay;
  unsigned short duration;
  unsigned short swgo;
} BCAST;

typedef struct ttcvi
{
  unsigned short ttc_Rom [0x40];  // Addr 0x00-0x7F
  unsigned short ttc_Csr1;        // Addr 0x80
  unsigned short ttc_Csr2;        // Addr 0x82
  unsigned short ttc_SwRst;       // Addr 0x84
  unsigned short ttc_SwL1a;       // Addr 0x86
  unsigned short ttc_EvtCountMsw; // Addr 0x88
  unsigned short ttc_EvtCountLsw; // Addr 0x8A
  unsigned short ttc_OrbitRst;    // Addr 0x8C
  unsigned short ttc_NotUsed0;    // Addr 0x8E
  BCAST          ttc_Bcast[4];  // Addr 0x90-0x97,0x98-0x9F,0xA0-0xA7,0xA8-0xAF
  unsigned int   ttc_Fifo[4];     // Addr 0xB0, 0xB4, 0xB8, 0xBC
  unsigned int   ttc_LongVmeCycl; // Addr 0xC0
  unsigned short ttc_ShortVmeCycl;// Addr 0xC4
  unsigned short ttc_NotUsed1;    // Addr 0xC6
  unsigned short ttc_TrigWordMsw; // Addr 0xC8
  unsigned short ttc_TrigWordLsw; // Addr 0xCA
} TTCVI;

#define TTCVI_SIZE sizeof(TTCVI)
#define TTCVI_OFFS 0xEE1000

/* ------------------------------------------------------------------------- */

int open_ttcvi( unsigned long *pttcvi_virtual_addr );
int close_ttcvi( void );

/* ------------------------------------------------------------------------- */

class TtcviModule: public TtcGenerator
{
public:
  TtcviModule( std::ostream &log = std::cout );
  ~TtcviModule();

  void l1a_set  ( )  { if( _ttcVi ) _ttcVi->ttc_SwL1a = 0; }
  void l1a_clear( )  { }
  void ecr_set  ( )  {
    // ShortCycle (= Broadcast Frame,
    // see TTCrx Reference Manual Rev 3.8 page 14 and page 24):
    // 0x02 = ECR, 0x01 = BCR, 0x3 = BCR+ECR
    if( _ttcVi ) _ttcVi->ttc_ShortVmeCycl = 0x02;
  }
  void ecr_clear( )  { }
  bool busy     ( )  { return false; }

private:
  TTCVI *_ttcVi;
};

/* ------------------------------------------------------------------------- */
#endif // TTCVIMODULE_H
