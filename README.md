## Description

The *l1atrigger* project provides a TTC-like tool with the ability
to generate L1A triggers and ECRs, taking into account a BUSY signal
if possible, without the need for any ATLAS TTC hardware modules,
although the a version of the tool also runs on an SBC in a VME-crate
with a TTCvi.

The tool, can run in 4 different configurations on 3 different platforms:
- on a RaspberryPI (model 3 or 4)
with a custom extension board to provide connections (and level translation)
for a L1A output, an ECR output and a BUSY input (L1A and ECR either connect
to an FPGA development board with a fiber output extension board that runs
firmware to generate the TTC fiber protocol,
- also on a RaspberryPI with its L1A signal connected to a TTCvi module L1A-input,
with the properly configured TTCvi module plus TTCvx module (for example)
generating the TTC fiber protocol (in that case no ECR can be generated),
- on the SBC (Single Board Computer) in a VME-crate with TTCvi plus TTCex
(or TTCvx), generating L1A triggers and ECRs under software control
through TTCvi register accesses (this configuration lacks an input for BUSY),
- on a ``picozed`` board (based on a Xilinx *Zynq* FPGA with on-chip
ARM processor) running a Linux OS,
with appropriate firmware (*this firmware is still under development*).

The BUSY signal in the configurations typically originates from a module
or a set of modules able to generate a (single) BUSY signal while doing 
data-acquisition, such as a FELIX card.

The main purpose of the tool is to generate L1A triggers
with a configurable rate, which is either constant or has
a certain random distribution with an average rate as selected.
If a BUSY signal is connected and is taken into account (by default it is),
L1A triggers stop being generated as long as the BUSY signal is asserted.
It is also possible to generate a given number of triggers, at the requested rate.

In case an ECR signal can be generated, this is done periodically with
a configurable period, in parallel with the L1A trigger generation
(but without ECR and L1A overlapping). 

These are the names of the tool for the 3 supported platforms:
- **pitrigger**: runs on a RaspberryPI Model 3B or 4B (it determines the model type and adjusts its internal hardware mapping accordingly)
- **vitrigger**: runs on an SBC in a VME-crate with TTCvi; requires access to ATLAS libraries (expects a TTCvi with a fixed base address).
- **zedtrigger**: runs on a picozed board (*under development*).

## Getting started

- clone the repository
- depending on the platform you are on and need the tool for, execute one of:
  - ``make -f Makefile-rpi``, produces executable ``pitrigger``
  - ``make -f Makefile-ttcvi``, produces executable ``vitrigger``
  - ``make -f Makefile-zed``, produces executable ``zedtrigger``
- run the tool for your platform, e.g. ``sudo ./pitrigger -f 10`` or
``vitrigger -f 10``

## More details

This is the help text of the tool:
```
$ ./pitrigger -h
--freq [-f] <n>      : Set L1A trigger rate to <n> [KHz] (default: 1)
                       (choose 0 to get the maximum rate the tool is capable of)
--nobusy [-B]        : Busy input disabled (default: enabled)
--calib [-c] <c>     : Set calibration constant to <c>: overhead in [ns]
                       (value displayed at startup as 'otherOverhead')
                       to allow some form of (fine)tuning the resulting L1A frequency
--ecrperiod [-E] <e> : Generate ECR with period <e> [ms] (default: 0 = no ECR)
--gauss [-g]         : Use gaussian spread instead of exponential in rate
--help [-h]          : Show this help text
--pulsewidth [-P] <p>: Set L1A pulse width to <p> [ns] (default: 100)
--random [-r]        : Enable (exponential) 'random' rate
--fstep [-S]         : Run performance test with this rate increase step in [KHz]
--time [-T] <t>      : Number of seconds <t> [s] to generate triggers (default: 0=endless)
                       or time interval between rate steps, with option -S (default: 10)
--trigcount [-t] <n> : Number of L1A triggers to generate (default: undefined)
```

The time interval between subsequent trigger pulses by default is constant,
but can be configured to follow an exponential distribution,
by providing option `-r`.
The average interval corresponds to the average trigger rate set as set
with option `-f`.

Alternatively, with the option `-g`, the trigger rate can be set to follow
a Gaussian distribution centered at the provided rate value with a standard
deviation equal to 10% of the rate.

In all cases the triggering can be stopped after a certain number of triggers
have been generated, which can be set with option `-t`.

The tool features functionality to measure a system's performance with respect
to the L1A trigger rate:
the trigger rate is incremented in steps of configurable duration and
frequency rate steps.
The trigger generation is halted when busy is received for at least half
the duration of a stage.
This function is enabled by supplying options ``-S`` and ``-T``.

Note that on the RaspberryPI platform the tool has to run as root
or with ``sudo``, because access to ``/dev/mem`` is required.

The time interval between two pulses is internally measured by either a call
to a ``sleep()`` function (if the selected rate is sufficiently low to
justify the overhead of calling ``sleep()``, or by polling the system clock time.
In both cases a fixed amount of 'loop' overhead is taken into account
(determined by running the tool on the selected platform).
but its value may be adjusted by providing a different value with option ``-c``.

L1A trigger generation and the ECR generation (if enabled)
each run in a separate thread, while the main thread monitors and displays
the L1A rate and BUSY percentage (which are updated every second).
