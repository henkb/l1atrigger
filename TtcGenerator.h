#ifndef TTCGENERATOR_H
#define TTCGENERATOR_H

#include <iostream>

// Pure virtual base class for TTC signal generators
class TtcGenerator
{
public:
  TtcGenerator( std::ostream &log )
    : _log( log ) { }
  virtual ~TtcGenerator() { }

  virtual void l1a_set  ( ) = 0;
  virtual void l1a_clear( ) = 0;
  virtual void ecr_set  ( ) = 0;
  virtual void ecr_clear( ) = 0;
  virtual bool busy     ( ) = 0;
  virtual int  l1a_gen  ( ) { return 0; }      // For auto-trigger
  virtual void l1a_pause ( ) { }               // For auto-trigger
  virtual void l1a_resume( ) { }               // For auto-trigger
  virtual uint32_t busy_count( ) { return 0; } // For auto-trigger

protected:
  std::ostream &_log;
};

#endif // TTCGENERATOR_H
