#ifndef L1ATHREAD_H
#define L1ATHREAD_H

#include <thread>
#include <iostream> // for std::cout

//class TtcGenerator;
#include "TtcGenerator.h"

#define L1A_DELAY_SZ    0x10000
#define L1A_RATE_CONST  0
#define L1A_RATE_RANDOM 1
#define L1A_RATE_GAUSS  2

class L1aThread
{
 public:
  L1aThread( TtcGenerator *gen, std::ostream &log = std::cout );
  virtual ~L1aThread( );

 public:
  // General
  void start         ( );
  void stop          ( );
  void pause         ( );
  void resume        ( );
  void run           ( );
  bool monitor       ( int seconds );
  void showSettings  ( );

  // L1A trigger management
  bool isRunning     ( )                { return _running; }
  void setAutoTrigger( bool b )         { _autoTrigger = b; }
  void setFrequency  ( double f )       { _triggerFreq = f; }
  void setMode       ( int m )          { _triggerMode = m; }
  void setPulseWidth ( int w )          { _pulseWidth = w; }
  void setBusyEnabled( bool b )         { _busyEnabled = b; }
  void setTriggerCount( uint32_t c )    { _triggerCount = c; }
  uint64_t triggers  ( )                { if(_autoTrigger) return _ttcGen->l1a_gen();
                                          else return _triggers; }
  uint64_t triggersSuppressed( )        { if(_autoTrigger) return 0;
                                          else return _triggersSuppressed; }

  // Timing
  inline void pause    ( uint64_t delay_ns, uint64_t *delay_start = 0 );
  int  sleepOverhead   ( );
  int  gettimeOverhead ( );
  void setSleepOverhead( int t )        { _sleepOverhead = t; }

 private:
  std::string timestamp();

 private:
  // General
  std::thread  *_pThread;
  std::ostream &_log;

  // Trigger management
  bool     _running;
  bool     _autoTrigger;
  // 'volatile' added for vitrigger (otherwise hanging state):
  volatile bool _doPause, _paused;
  double   _triggerFreq;
  int      _triggerMode; // 0: constant 1: random 2: gaussian
  uint32_t _triggerCount;
  int      _pulseWidth;
  bool     _busyEnabled;
  // An array of values to provide for distributions in delays:
  uint64_t _delay[L1A_DELAY_SZ];

  // Timing
  int _sleepOverhead;   // Overhead in time of usleep()/sleep_for()
  int _gettimeOverhead; // Overhead in time of clock_gettime() (not used, yet)

  // Statistics
  uint64_t _triggers;
  uint64_t _triggersSuppressed;

  TtcGenerator *_ttcGen;
};
#endif // L1ATHREAD_H
