// C. A. Gottardo, J. Vermeulen, H. Boterenbrood NIKHEF, 2019-2020
// For the benefit of the FELIX users & developers
// Redesigned by Henk B, June 2020

#include "L1aThread.h"
#include "EcrThread.h"
#include <getopt.h>
#include <csignal>
using namespace std;

#ifdef _TTCVI_
#include "TtcviModule.h"
#elif _ZED_
#include "ZedBoard.h"
#else
#include "RaspberryIO.h"
#endif

void sigint_handler( int signal )
{
#ifdef _ZED_
  ZedBoard zed; // C/D'tor will halt (auto)triggers
#endif // _ZED_
  exit( 1 );
}

void print_help()
{
  cout <<
#ifdef _ZED_
    "--noautotrig [-A]    : Use software-controlled L1A trigger (default: automatic).\n"
    "--clockconfig [-C]   : Initialize the ZED-board Si5338 clock chip, and exit.\n"
#endif // _ZED_
    "--freq [-f] <n>      : Set L1A trigger rate to <n> [KHz] (default: 1)\n"
    "                       (choose 0 for maximum rate the tool is capable of).\n"
    "--nobusy [-B]        : Busy input ignored (default: inhibits triggering).\n"
    "--ecrperiod [-E] <e> : Generate ECR with period <e> [ms] (default: 0 = no ECR).\n"
    "--ecrinhibit [-e] <i>: Trigger inhibit before and after the ECR, in [ms] (default: 1).\n"
    "--gauss [-g]         : Use gaussian spread instead of exponential in rate.\n"
    "--help [-h]          : Show this help text.\n"
    "--pulsewidth [-P] <p>: Set L1A pulse width to <p> [ns] (default: 100).\n"
    "--random [-r]        : Enable 'random' rate (exponential spread).\n"
    "--fstep [-S]         : Run increasing rate test with this rate step, in [KHz].\n"
    "--time [-T] <t>      : Number of seconds <t> to generate triggers "
    "(default: 0=endless)\n"
    "                       or time interval between rate steps, "
    "with option -S (default: 10).\n"
    "--trigcount [-t] <n> : Number of L1A triggers to generate "
    "(default: unlimited).\n";
}

int main(int argc, char *argv[])
{
  int      opt;
  double   freq_khz       = 1.0;
  int      duration_s     = 0;
  bool     busy_enabled   = true;
  bool     do_random      = false;
  bool     do_gauss       = false;
  int      mode           = L1A_RATE_CONST;
  int      pulse_width_ns = -1;
  double   step_khz       = 0.0;
  int      ecr_period_ms  = 0;
  int      ecr_inhibit_ms = 1;
  uint32_t trig_count     = 0;
  bool     autotrigger    = false;
  bool     clockconfig    = false;

  // Parse the options
#ifdef _ZED_
  autotrigger = true;
  pulse_width_ns = 0;
  const char* const short_opts = "ABCE:e:f:ghP:rS:T:t:";
  const option long_opts[] = {
    {"noautotrig", no_argument,       nullptr, 'A'},
    {"clockconfig",no_argument,       nullptr, 'C'},
#else
  const char* const short_opts = "BE:e:f:ghP:rS:T:t:";
  const option long_opts[] = {
#endif // _ZED_
    {"nobusy",     no_argument,       nullptr, 'B'},
    {"ecrperiod",  required_argument, nullptr, 'E'},
    {"ecrinhibit", required_argument, nullptr, 'e'},
    {"freq",       required_argument, nullptr, 'f'},
    {"gauss",      no_argument,       nullptr, 'g'},
    {"help",       no_argument,       nullptr, 'h'},
    {"pulsewidth", required_argument, nullptr, 'P'},
    {"random",     no_argument,       nullptr, 'r'},
    {"fstep",      required_argument, nullptr, 'S'},
    {"time",       required_argument, nullptr, 'T'},
    {"trigcount",  required_argument, nullptr, 't'},
    {nullptr,      no_argument,       nullptr, 0}
  };

  while( (opt = getopt_long(argc, argv, short_opts, long_opts, nullptr)) != -1 )
    {
      switch (opt)
        {
        case 'A':
          autotrigger = false;
          break;

        case 'B':
          busy_enabled = false;
          break;

        case 'C':
          clockconfig = true;
          break;

        case 'E':
          if( sscanf(optarg, "%d", &ecr_period_ms) != 1 )
            cout << "-E: error in argument" << endl;
          break;

        case 'e':
          if( sscanf(optarg, "%d", &ecr_inhibit_ms) != 1 )
            cout << "-e: error in argument" << endl;
          break;

        case 'f':
          if( sscanf(optarg, "%lf", &freq_khz) != 1 )
            cout << "-f: error in argument" << endl;
          break;

        case 'g':
          do_gauss = true;
          break;

        case 'P':
          if( sscanf(optarg, "%d", &pulse_width_ns) != 1 )
            cout << "-P: error in argument" << endl;
          break;

        case 'r':
          do_random = true;
          break;

        case 'S':
          if( sscanf(optarg, "%lf", &step_khz) != 1 )
            cout << "-S: error in argument" << endl;
          break;

        case 'T':
          if( sscanf(optarg, "%d", &duration_s) != 1 )
            cout << "-d: error in argument" << endl;
          break;

        case 't':
          if( sscanf(optarg, "%u", &trig_count) != 1 )
            cout << "-t: error in argument" << endl;
          break;

        case 'h':
        case '?':
        default:
          print_help();
          return 1;
        }
    }

  TtcGenerator *ttcGenerator;
#ifdef _TTCVI_
  ttcGenerator = new TtcviModule;
#elif _ZED_
  ZedBoard *zed = new ZedBoard;
  ttcGenerator = zed;
  if( clockconfig )
    {
      cout << "Configuring Si5338..." << endl;
      zed->config_si5338();
      cout << "Si5338 done" << endl;
      return 0;
    }
#else
  ttcGenerator = new RaspberryIO;
#endif

  L1aThread l1aThread( ttcGenerator );

#ifdef _TTCVI_
  // Fixed sleep() overhead value: better results.. (on Nikhef SBC at least)
  l1aThread.setSleepOverhead( 350000 );
#endif

  // Configure
  l1aThread.setAutoTrigger( autotrigger );

  l1aThread.setFrequency( freq_khz );

  l1aThread.setBusyEnabled( busy_enabled );

  if( do_random && !do_gauss )
    mode = L1A_RATE_RANDOM;
  else if( do_gauss )
    mode = L1A_RATE_GAUSS;
  else
    mode = L1A_RATE_CONST;
  l1aThread.setMode( mode );

  if( pulse_width_ns >= 0 )
    l1aThread.setPulseWidth( pulse_width_ns );

  l1aThread.setTriggerCount( trig_count );

  // Display the configuration
  l1aThread.showSettings();

  // NB: we need to build in the capability to pause the L1A triggers
  //     while the ECR is generated (at the same time this will fix
  //     the issue of both threads L1aThread and EcrThread accessing
  //     the same hardware (registers); hence l1aThread as parameter)
  EcrThread ecrThread( ttcGenerator, &l1aThread );
  ecrThread.setInhibit( ecr_inhibit_ms );
  ecrThread.setPeriod( ecr_period_ms );
  if( ecr_period_ms > 0 )
    ecrThread.showSettings();
  if( pulse_width_ns >= 1000 )
    // Extra: also adjust ECR pulse width if required
    // (which is set in units of microseconds)
    ecrThread.setPulseWidth( pulse_width_ns/1000 );
  ecrThread.start();

  std::signal( SIGINT, sigint_handler );

  // Run
  if( step_khz > 0.0 )
    {
      if( duration_s == 0 )
        duration_s = 10; // Default for frequency ramp operation

      cout << "Start ramping..." << endl;
      bool ramping = true;
      while( ramping )
        {
#ifdef _ZED_
          zed->autotrigger( autotrigger, busy_enabled, mode, freq_khz );
#endif // _ZED_
          l1aThread.start();
          ramping = l1aThread.monitor( duration_s );
          l1aThread.stop();

          freq_khz += step_khz;
          if( ramping )
            cout << "To " << freq_khz << " kHz:" << endl;
          l1aThread.setFrequency( freq_khz );
        }
      cout << "Ramp halted" << endl;
    }
  else
    {
#ifdef _ZED_
      zed->autotrigger( autotrigger, busy_enabled, mode, freq_khz, trig_count );
#endif // _ZED_
      l1aThread.start();
      l1aThread.monitor( duration_s );
      l1aThread.stop();
    }

  ecrThread.stop();

  cout << "Totals: triggers " << l1aThread.triggers() << " (0x"
       << hex << uppercase << l1aThread.triggers() << ")" << dec;
  if( l1aThread.triggersSuppressed() > 0 )
    cout << " (suppressed " << l1aThread.triggersSuppressed() << ")";
  cout << endl;
  if( ecrThread.ecrCount() > 0 )
    cout << "        ECRs " << ecrThread.ecrCount() << " (0x"
         << hex << uppercase << ecrThread.ecrCount() << ")" << dec << endl;

  delete ttcGenerator;

  return 0;
}
