#include <ctime>
#include "EcrThread.h"
#include "L1aThread.h"
#include "TtcGenerator.h"

// ----------------------------------------------------------------------------

EcrThread::EcrThread( TtcGenerator *gen, L1aThread *l1a, std::ostream &log )
  : _pThread( 0 ),
    _log( log ),
    _l1aThread( l1a ),
    _running( false ),
    _ecrPeriodMs( 0 ),
    _ecrInhibitMs( 1 ),
    _pulseWidthUs( 1 ),
    _sleepOverhead( 0 ),
    _ecrCount( 0 ),
    _ttcGen( gen )
{
  // Dynamically determine the usleep() call overhead
  _sleepOverhead = this->sleepOverhead();
}

// ----------------------------------------------------------------------------

EcrThread::~EcrThread()
{
  // In case the thread is still running
  this->stop();
}

// ----------------------------------------------------------------------------

void EcrThread::start()
{
  //_log << "(DEBUG: EcrThread::start)" << std::endl;
  if( _pThread == 0 )
    _pThread = new std::thread( [this](){ this->run(); } );

  // Can delete thread at our convenience
  _pThread->detach();
}

// ----------------------------------------------------------------------------

void EcrThread::stop()
{
  //_log << "(DEBUG EcrThread::stop)" << std::endl;
  _running = false;

  if( _pThread )
    {
      // Wait until the receive thread (i.e. function run()) exits
      //_pThread->join();
      delete _pThread;
      _pThread = 0;
    }
}

// ----------------------------------------------------------------------------

void EcrThread::run()
{
  //_log << "(EcrThread started)" << std::endl;

  _ecrCount = 0;
  if( _ecrPeriodMs > 0 )
    _running = true;
  else
    _running = false;

  // First ECR is generated immediately
  while( _running )
    {
      if( _ttcGen )
        {
          if( _l1aThread )
            _l1aThread->pause(); // Pause L1A triggers

          pause( _ecrInhibitMs*1000 );

          _ttcGen->ecr_set();
        }

      pause( _pulseWidthUs );
      //pause( 50000 ); // 50ms TEST

      if( _ttcGen )
        {
          _ttcGen->ecr_clear();

          pause( _ecrInhibitMs*1000 );

          if( _l1aThread )
            _l1aThread->resume(); // Resume L1A triggers
        }

      ++_ecrCount;
      _log << timestamp() << " ECR=" << _ecrCount << std::endl;

      if( _ecrPeriodMs*1000 > _pulseWidthUs + _ecrInhibitMs*2*1000 )
        pause( _ecrPeriodMs*1000 - _pulseWidthUs - _ecrInhibitMs*2*1000 );
    }
}

// ----------------------------------------------------------------------------

void EcrThread::showSettings()
{
  _log << "ECR period        = " << _ecrPeriodMs   << " ms" << std::endl;
  _log << "ECR pulseWidth    = " << _pulseWidthUs  << " us" << std::endl
       << "ECR sleepOverhead = " << _sleepOverhead << " us" << std::endl;
}

// ----------------------------------------------------------------------------
// Private functions
// ----------------------------------------------------------------------------

void EcrThread::pause( uint64_t delay_us )
{
  // Use usleep() when appropriate, but run a busy-wait loop for short delays
  // (as determined by usleep() overhead) to achieve better timing precision
  // for shorter delays
  if( delay_us > _sleepOverhead )
    {
      std::this_thread::sleep_for( std::chrono::microseconds( delay_us -
                                                              _sleepOverhead) );
    }
  else if( delay_us > 0 )
    {
      struct timespec ts1, ts2;
      volatile long s, t;
      clock_gettime( CLOCK_REALTIME, &ts1 );
      s = ts1.tv_sec*1000000000 + ts1.tv_nsec;
      clock_gettime( CLOCK_REALTIME, &ts2 );
      t = (ts2.tv_sec*1000000000 + ts2.tv_nsec) - s; // Nanoseconds
      while( t < delay_us*1000 )
        {
          clock_gettime( CLOCK_REALTIME, &ts2 );
          t = (ts2.tv_sec*1000000000 + ts2.tv_nsec) - s; // Nanoseconds
        }
    }
}

// ----------------------------------------------------------------------------

int EcrThread::sleepOverhead()
{
  // Determine the time overhead it takes to execute usleep(1) or sleep_for
  struct timespec ts1, ts2;
  clock_gettime( CLOCK_REALTIME, &ts1 );
  // Average over 4 calls..
  //usleep( 1 ); usleep( 1 ); usleep( 1 ); usleep( 1 );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  clock_gettime( CLOCK_REALTIME, &ts2 );
  // t in nanoseconds
  long t = ((ts2.tv_sec*1000000000 + ts2.tv_nsec) -
            (ts1.tv_sec*1000000000 + ts1.tv_nsec))/4;

  // Subtract the 1000 ns sleep time and return as micro- or nanoseconds
  return (t-1000000+500)/1000; // Return microseconds
  //return (t-1000000);        // Return nanoseconds
}

// ----------------------------------------------------------------------------

std::string EcrThread::timestamp()
{
  time_t now;
  char the_date[24];
  now = time(NULL);
  strftime(the_date, 24, "[%H:%M:%S %d.%m.%Y]", localtime(&now));
  return std::string(the_date);
}

// ----------------------------------------------------------------------------
