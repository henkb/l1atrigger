#include "ZedBoard.h"

#include <fstream>
#include <sstream>
#include <sys/stat.h> // for stat()
#include <sys/mman.h> // for mmap()
#include <iomanip>
//#include <stdexcept>
#include <random>
#include <cmath> // for log10()

// For dealing with /dev/gpiochipX:
// (faster alternative, i.e. class GpioIoctl)
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
//#include <linux/gpio.h>
#include <cstring>

#define LUT_SZ 4096

// ----------------------------------------------------------------------------
// Class ZedBoard
// ----------------------------------------------------------------------------

ZedBoard::ZedBoard( std::ostream &log )
  : TtcGenerator( log ),
    _gpio( 0 ),
    _autoTrigger( false )
{
  //_gpio = new GPIO( log );
  //_gpio = new GpioIoctl( log );
  _gpio = new RegsMem( log );

  _gpio->enable( ZED_L1A_OUT, DIR_OUT );

  _gpio->enable( ZED_ECR_OUT, DIR_OUT );

  _gpio->enable( ZED_BUSY_IN, DIR_IN );

  this->busy(); // Read once to get input level right(?)

  // Configure firmware initially for manual/software control
  _gpio->setVal( REG_TTC_CONFIG, 0 );
  _gpio->setVal( REG_L1A_AUTO_PAUSE, 1 );
}

// ----------------------------------------------------------------------------

ZedBoard::~ZedBoard()
{
  if( _gpio != 0 )
    {
      _gpio->setVal( REG_L1A_AUTO_PAUSE, 1 );
      _gpio->setVal( REG_TTC_CONFIG, 0 ); // Software/manual control
      _gpio->disable( ZED_L1A_OUT );
      _gpio->disable( ZED_ECR_OUT );
      _gpio->disable( ZED_BUSY_IN );
      delete _gpio;
      _gpio = 0;
    }
}

// ----------------------------------------------------------------------------

void ZedBoard::autotrigger( bool autotrigger, bool busy_enabled, int mode,
                            double freq_khz, uint32_t trig_count )
{
  _autoTrigger = autotrigger;
  if( !autotrigger )
    {
      _gpio->setVal( REG_TTC_CONFIG, 0 );
      return;
    }

  // Fill LUT
  uint32_t d;
  if( mode == 0 || mode > 2 ) // L1A_RATE_CONST
    {
      // Constant (LUT-based)
      if( freq_khz != 0.0 )
        d = (uint32_t) (40079000.0/(freq_khz*1000.0));
      else
        d = (uint32_t) 1; // Going for maximum rate
      for( int i=0; i<LUT_SZ; ++i )
        {
          //if( i > 4000 ) // DEBUG
          //_gpio->setLut( i, 2*d );
          //else
          // Delay in units of clock period
          _gpio->setLut( i, d );

          // DEBUG
          //if( i < 20 )
          //_log << i << ": " << d << " LUT=" << _gpio->getLut(i) << std::endl;
        }
    }
  else if( mode == 1 ) // L1A_RATE_RANDOM
    {
      // Random distribution (exponential)
      std::uniform_real_distribution<double> distr( 0.0000001, 1.0 );
      std::random_device randev;
      uint32_t d_avg = (uint32_t) (40079000.0/(freq_khz*1000.0));
      for( int i=0; i<LUT_SZ; ++i )
        {
          // Delay in in units of clock period
          d = (uint32_t) (-log(distr(randev)) * (double) d_avg);
          if( d == 0 ) d = 1;
          _gpio->setLut( i, d );

          // DEBUG
          //if( i < 20 )
          //_log << i << ": " << d << " LUT=" << _gpio->getLut(i) << std::endl;
        }
    }
  else if( mode == 2 ) // L1A_RATE_GAUSS
    {
      // Gaussian distribution
      // (68% of the time the rate variation is within 10%)
      double hi = freq_khz + 0.1*freq_khz;
      double lo = freq_khz - 0.1*freq_khz;
      double sigma = (hi - lo)/2.0;
      std::normal_distribution<double> distr( freq_khz, sigma );
      std::random_device randev;
      for( int i=0; i<LUT_SZ; ++i )
        {
          // Delay in units of clock period
          d = (uint32_t) (40079000.0/(distr(randev)*1000.0));
          _gpio->setLut( i, d );

          // DEBUG
          //if( i < 20 )
          //_log << i << ": " << d << " LUT=" << _gpio->getLut(i) << std::endl;
        }
    }

  // Configure
  _gpio->setVal( REG_BCR_PERIOD, 3564 );
  if( trig_count > 0 )
    {
      _gpio->setVal( REG_L1A_COUNT, trig_count );
      _gpio->setVal( REG_LUT_CONT_ENABLE, 0 );
    }
  else
    {
      // (###Commented: USING LUT-MODE FOR L1A_RATE_CONST TOO:)
      //if( mode == 0 ) // L1A_RATE_CONST
      //  _gpio->setVal( REG_L1A_COUNT, 0 );
      //else
        // LUT-mode: use all LUT positions
        _gpio->setVal( REG_L1A_COUNT, LUT_SZ );
      _gpio->setVal( REG_LUT_CONT_ENABLE, 1 );
    }
  // Period in units of clock period (for non-LUT based trigger generation)
  //double period = 40079000.0/(freq_khz*1000.0) + 0.5;
  double period;
  if( freq_khz != 0.0 )
    period = 40079000.0/(freq_khz*1000.0);
  else
    period = 1; // Going for maximum rate
  _gpio->setVal( REG_L1A_PERIOD, (int) period );
  _gpio->setVal( REG_BUSY_ENABLE, (busy_enabled ? 1 : 0) );

  // Start automatic L1A trigger,
  // running at a constant rate or with LUT-based inter-L1A delays
  // (###Commented: USING LUT-MODE FOR L1A_RATE_CONST TOO:)
  //if( mode == 0 ) // L1A_RATE_CONST
  //  _gpio->setVal( REG_TTC_CONFIG, 1 );
  //else
    _gpio->setVal( REG_TTC_CONFIG, 2 );
  _gpio->setVal( REG_L1A_AUTO_PAUSE, 0 );
}

// ----------------------------------------------------------------------------

void ZedBoard::l1a_set()
{
  if( _autoTrigger || !_gpio ) return;
  _gpio->setVal( ZED_L1A_OUT, 1 );
}

// ----------------------------------------------------------------------------

void ZedBoard::l1a_clear()
{
  if( _autoTrigger || !_gpio ) return;
  _gpio->setVal( ZED_L1A_OUT, 0 );
}

// ----------------------------------------------------------------------------

void ZedBoard::ecr_set()
{
  if( !_gpio ) return;
  _gpio->setVal( ZED_ECR_OUT, 1 );
}

// ----------------------------------------------------------------------------

void ZedBoard::ecr_clear()
{
  if( !_gpio ) return;
  _gpio->setVal( ZED_ECR_OUT, 0 );
}

// ----------------------------------------------------------------------------

bool ZedBoard::busy()
{
  if( !_gpio ) return false;
  return( _gpio->getVal( ZED_BUSY_IN ) == 0 );
}

// ----------------------------------------------------------------------------

int ZedBoard::l1a_gen()
{
  // Return number of auto-generated L1A
  if( !_autoTrigger || !_gpio ) return 0;
  return( _gpio->getVal( REG_L1A_COUNT_READ ) );
}

// ----------------------------------------------------------------------------

void ZedBoard::l1a_pause()
{
  if( !_gpio ) return;
  _gpio->setVal( REG_L1A_AUTO_PAUSE, 1 );
}

// ----------------------------------------------------------------------------

void ZedBoard::l1a_resume()
{
  if( !_gpio ) return;
  _gpio->setVal( REG_L1A_AUTO_PAUSE, 0 );
}

// ----------------------------------------------------------------------------

uint32_t ZedBoard::busy_count()
{
  // Return the BUSY-active counter
  if( !_autoTrigger || !_gpio ) return 0;
  return( (uint32_t) _gpio->getVal( REG_BUSY_PULSE_COUNT ) );
}

// ----------------------------------------------------------------------------

// File si5338-config-xxx.h, containing all i2cset/get commands to configure
// the Si5338 device, as a string, created as follows:
// ./si5338-config-gen.awk -v INCLUDEIT=1 Si5338-RevB-Registers_240P474.h
//                         > si5338-config-240P474.h
const char *config_ops =
#include "Si5338/si5338-config-240P474.h"

#include <stdlib.h> // For system()
#include <string.h> // For strlen()

void ZedBoard::config_si5338()
{
  // NB: may as well do: system( config_ops );
  const std::string ops( config_ops );
  std::istringstream iss( ops );
  char line[256];
  int  line_nr = 0;
  while( !iss.eof() )
    {
      iss.getline( line, sizeof(line) );
      ++line_nr;
      // Execute the command on this line, if any
      if( strlen(line) > 0 && line[0] != '#' )
        {
          int s = system( line );
          if( s != 0 )
            _log << "Line " << line_nr
                 << ", result=" << s << ": " << line << std::endl;
        }
    }

  // Reset GTH
  _gpio->resetGth();
}

// ----------------------------------------------------------------------------
// Class GPIO
// ----------------------------------------------------------------------------
/*
static const char CLASS_ROOT[] = "/sys/class/gpio";
static const char EXPORT[]     = "export";
static const char UNEXPORT[]   = "unexport";
static const char VALUE[]      = "value";
static const char DIRECTION[]  = "direction";

// ----------------------------------------------------------------------------

GPIO::GPIO( std::ostream &log )
  : _log( log )
{
}

// ----------------------------------------------------------------------------

GPIO::~GPIO()
{
}

// ----------------------------------------------------------------------------

void GPIO::enable( int pin, Direction dir )
{
  this->enable( pin );
  this->setDirection( pin, dir );
}

// ----------------------------------------------------------------------------

void GPIO::enable( int pin ) const
{
  // Already enabled?
  if( pinExported(pin) )
    return;

  std::stringstream ss;
  ss << CLASS_ROOT << '/' << EXPORT;
  write( ss.str(), std::to_string(pin) );

  // Check result
  if( !pinExported(pin) )
    {
      std::stringstream ess;
      ess << "###Could not export GPIO " << pin;
      //throw std::runtime_error(ess.str());
      _log << ess.str() << std::endl;
    }
}

// ----------------------------------------------------------------------------

void GPIO::disable( int pin ) const
{
  // Already disabled?
  if( !pinExported(pin) )
    return;

  std::stringstream ss;
  ss << CLASS_ROOT << '/' << UNEXPORT;
  write( ss.str(), std::to_string(pin) );

  // Check result
  if( pinExported(pin) )
    {
      std::stringstream ess;
      ess << "###Could not unexport GPIO " << pin;
      //throw std::runtime_error(ess.str());
      _log << ess.str() << std::endl;
    }
}

// ----------------------------------------------------------------------------

void GPIO::setDirection( int pin, Direction dir )
{
  if( pinNotOkay( pin ) )
    return;

  switch( dir )
    {
    case DIR_IN:
      write( pinToFilename(pin, DIRECTION), "in" );
      break;
    case DIR_OUT:
      write( pinToFilename(pin, DIRECTION), "out" );
      break;
    default:
      //throw std::runtime_error( "Unknown pin direction" );
      _log << "###Unknown pin direction" << std::endl;
    }

  // Open an input or output stream and keep it available
  // for subsequent setVal() and getVal() operations
  if( dir == DIR_IN )
    {
      _inputs[pin] = new std::ifstream( pinToFilename(pin, VALUE),
                                        std::ifstream::in );
      if( !_inputs[pin]->is_open() )
        _inputs.erase( pin );
    }
  else if( dir == DIR_OUT )
    {
      _outputs[pin] = new std::ofstream( pinToFilename(pin, VALUE),
                                         std::ofstream::out );
      if( !_outputs[pin]->is_open() )
        _outputs.erase( pin );
    }
}

// ----------------------------------------------------------------------------

Direction GPIO::direction( int pin ) const
{
  if( pinNotOkay( pin ) )
    return DIR_UNKNOWN;

  std::string rd = read( pinToFilename(pin, DIRECTION) );

  Direction dir = DIR_UNKNOWN;
  if( !rd.empty() )
    {
      if( rd == "in" )
        dir = DIR_IN;
      else if( rd == "out" )
        dir = DIR_OUT;
    }
  return dir;
}

// ----------------------------------------------------------------------------

int GPIO::get( int pin ) const
{
  if( pinNotOkay( pin ) )
    return 0;

  std::string rd = read( pinToFilename(pin, VALUE) );

  int val = -1;
  if( !rd.empty() )
    {
      if( rd[0] == '0' )
        val = 0;
      else if( rd[0] == '1' )
        val = 1;
    }
  return val;
}

// ----------------------------------------------------------------------------

void GPIO::set( int pin, int value ) const
{
  if( pinNotOkay( pin ) )
    return;

  write( pinToFilename(pin, VALUE), std::to_string(value) );
}

// ----------------------------------------------------------------------------

int GPIO::getVal( int pin ) const
{
  std::string rd;
  try {
    std::ifstream *ifs = _inputs.at( pin );
    std::getline( *ifs, rd );
  }
  catch( const std::out_of_range& oor ) {
    _log << "###Invalid input, pin " << pin << std::endl;
    return -1;
  }

  int val = -1;
  if( !rd.empty() )
    {
      if( rd[0] == '0' )
        val = 0;
      else if( rd[0] == '1' )
        val = 1;
    }
  return val;
}

// ----------------------------------------------------------------------------

void GPIO::setVal( int pin, int value ) const
{
  try {
    std::ofstream *ofs = _outputs.at( pin );
    *ofs << std::to_string( value ) << std::flush;
  }
  catch( const std::out_of_range& oor ) {
    _log << "###Invalid output, pin " << pin << std::endl;
  }
}

// ----------------------------------------------------------------------------
// Class GPIO private functions
// ----------------------------------------------------------------------------

void GPIO::write( const std::string & filename,
                  const std::string & content ) const
{
  std::ofstream ofs( filename, std::ofstream::out );
  if (!ofs.is_open())
    {
      std::stringstream ss;
      ss << "###Failed to open file " << filename;
      //throw std::runtime_error(ss.str());
      _log << ss.str() << std::endl;
      return;
    }
  ofs << content;
  ofs.close();
}

// ----------------------------------------------------------------------------

std::string GPIO::read( const std::string & filename ) const
{
  std::ifstream ifs( filename, std::ifstream::in );
  if( !ifs.is_open() )
    {
      std::stringstream ss;
      ss << "###Failed to open file " << filename;
      //throw std::runtime_error(ss.str());
      _log << ss.str() << std::endl;
      return std::string();
    }

  std::string rd;
  std::getline( ifs, rd );
  ifs.close();

  return rd;
}

// ----------------------------------------------------------------------------

bool GPIO::pinNotOkay( int pin ) const
{
  if( !pinExported(pin) )
    {
      std::stringstream ss;
      ss << "###Failed to use pin " << pin << ", not exported";
      //throw std::runtime_error(ss.str());
      _log << ss.str() << std::endl;
      return true;
    }
  return false;
}

// ----------------------------------------------------------------------------

bool GPIO::pinExported( int pin ) const
{
  std::string valFile = pinToFilename( pin, VALUE );
  struct stat buffer;
  return( stat( valFile.c_str(), &buffer) == 0 ); 
}

// ----------------------------------------------------------------------------

std::string GPIO::pinToFilename( int pin, const char *attribute ) const
{
  std::stringstream ss;
  ss << CLASS_ROOT << "/gpio" << pin << "/" << attribute;
  return ss.str();
}

// ----------------------------------------------------------------------------
// Class GpioIoctl (GPIO operation through ioctl() on /dev/gpiochipX)
// ----------------------------------------------------------------------------

GpioIoctl::GpioIoctl( std::ostream &log, int chipnr )
  : _fdGpioChip( -1 ),
    _log( log )
{
  std::ostringstream oss;
  oss << "/dev/gpiochip" << chipnr;

  struct gpiochip_info cinfo;
  _fdGpioChip = open( oss.str().c_str(), 0 );

  if( _fdGpioChip != -1 )
    {
      int ret = ioctl( _fdGpioChip, GPIO_GET_CHIPINFO_IOCTL, &cinfo );
      _log << "GPIO chip: " << cinfo.name << ", \"" << cinfo.label 
           << "\", " << cinfo.lines << " GPIO lines" << std::endl;
    }
}

// ----------------------------------------------------------------------------

GpioIoctl::~GpioIoctl()
{
  if( _fdGpioChip != -1 )
    close( _fdGpioChip );
}

// ----------------------------------------------------------------------------

void GpioIoctl::enable( int pin, Direction dir )
{
  if( _fdGpioChip == -1 ) return;

  struct gpiohandle_request req;
  // First GPIO happens to be set to 1014 for chip #1
  req.lineoffsets[0] = pin - ZED_GPIO_OFFS;
  req.lines          = 1;
  if( dir == DIR_IN )
    req.flags = GPIOHANDLE_REQUEST_INPUT;
  else if( dir == DIR_OUT )
    req.flags = GPIOHANDLE_REQUEST_OUTPUT;
  else
    {
      _log << "###Unknown direction for pin " << pin << std::endl;
      return;
    }
  req.default_values[0] = 0;
  std::ostringstream oss;
  oss << "Pin" << pin;
  strcpy( req.consumer_label, oss.str().c_str() );
  int ret = ioctl( _fdGpioChip, GPIO_GET_LINEHANDLE_IOCTL, &req );
  if( ret == -1 )
    {
      _log << "###Enable pin " << pin << ": " << std::flush;
      perror(0);
      return;
    }
  _fdGpio[pin] = req.fd;
}

// ----------------------------------------------------------------------------

int GpioIoctl::getVal( int pin ) const
{
  int fd;
  try {
    fd = _fdGpio.at( pin );
  }
  catch( const std::out_of_range& oor ) {
    _log << "###Invalid input, pin " << pin << std::endl;
    return 0;
  }
  struct gpiohandle_data data;
  int ret = ioctl( fd, GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data );
  if( ret == -1 )
    return 0;
  return( data.values[0] );
}

// ----------------------------------------------------------------------------

void GpioIoctl::setVal( int pin, int value ) const
{
  int fd;
  try {
    fd = _fdGpio.at( pin );
  }
  catch( const std::out_of_range& oor ) {
    _log << "###Invalid output, pin " << pin << std::endl;
    return;
  }
  struct gpiohandle_data data;
  data.values[0] = value;
  int ret = ioctl( fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data );
}

// ----------------------------------------------------------------------------
// Class GpioMem (GPIO operation through a memory-mapped Xilinx GPIO IP core)
// ----------------------------------------------------------------------------

// GPIO register indices
#define GPIO_DATA  0
#define GPIO_TRI   1
#define GPIO2_DATA 2
#define GPIO2_TRI  3
#define GDATA      GPIO2_DATA
#define GDIR       GPIO2_TRI

GpioMem::GpioMem( std::ostream &log )
  : _gpioRegs( 0 ),
    _log( log )
{
  // Open /dev/mem
  int mem_fd;
  if( (mem_fd = open( "/dev/mem", O_RDWR|O_SYNC )) < 0 ) {
    printf( "can't open /dev/mem \n" );
    exit( -1 );
  }

  // mmap GPIO registers
  void *gpio_map = mmap(
      NULL,             // Any adddress in our space will do
      (64*1024),         // Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       // Shared with other processes
      mem_fd,           // File to memory file descriptor
      0x41200000        // Offset to GPIO peripheral
  );

  close( mem_fd ); // No need to keep mem_fd open after mmap()

  if( gpio_map == MAP_FAILED ) {
    printf( "mmap error %d\n", (int)(size_t)gpio_map ); // errno also set!
    exit( -1 );
  }

  _gpioRegs = (volatile uint32_t *) gpio_map;

  this->enable( 1014+3, DIR_OUT );
  this->enable( 1014+4, DIR_OUT );
  this->enable( 1014+5, DIR_OUT );
  _gpioRegs[1] = 0;
  _log << std::hex << std::setfill('0')
       << "data=" << std::setw(8) << _gpioRegs[0]
       << " dir=" << std::setw(8) << _gpioRegs[1]
       << " data=" << std::setw(8) << _gpioRegs[2]
       << " dir=" << std::setw(8) << _gpioRegs[3]
       << std::dec << std::setfill(' ') << std::endl;
}

// ----------------------------------------------------------------------------

GpioMem::~GpioMem()
{
}

// ----------------------------------------------------------------------------

void GpioMem::enable( int pin, Direction dir )
{
  if( _gpioRegs == 0 ) return;

  pin = pin - ZED_GPIO_OFFS;

  if( dir == DIR_IN )
    _gpioRegs[GDIR] |= (1 << pin);
  else if( dir == DIR_OUT )
    _gpioRegs[GDIR] &= ~(1 << pin);
  else
    {
      _log << "###Unknown direction for pin " << pin << std::endl;
      return;
    }
}

// ----------------------------------------------------------------------------

int GpioMem::getVal( int pin ) const
{
  if( _gpioRegs == 0 ) return 0;

  pin = pin - ZED_GPIO_OFFS;

  if( _gpioRegs[GDATA] & (1 << pin) )
    return 1;
  else
    return 0;
}

// ----------------------------------------------------------------------------

void GpioMem::setVal( int pin, int value ) const
{
  if( _gpioRegs == 0 ) return;

  pin = pin - ZED_GPIO_OFFS;

  if( value == 1 )
    _gpioRegs[GDATA] |= (1 << pin); 
  else
    _gpioRegs[GDATA] &= ~(1 << pin); 
}
*/
// ----------------------------------------------------------------------------
// Class RegsMem (a memory-mapped register bank,
// and a Look-Up-Table (LUT) for trigger delays,
// plus an additional region for LTI stuff)
// ----------------------------------------------------------------------------

RegsMem::RegsMem( std::ostream &log )
  : _regs( 0 ),
    _lut( 0 ),
    _lti( 0 ),
    _log( log )
{
  // Open /dev/mem
  int mem_fd;
  if( (mem_fd = open( "/dev/mem", O_RDWR|O_SYNC )) < 0 ) {
    printf( "### Failed to open /dev/mem \n" );
    exit( -1 );
  }

  // GPIO registers mmap
  void *reg_map = mmap(
      NULL,             // Any adddress in our space will do
      1024,             // Map length
      PROT_READ|PROT_WRITE,// Enable reading & writing to mapped memory
      MAP_SHARED,       // Shared with other processes
      mem_fd,           // File-to-memory file descriptor
      //0x43C00000      // Offset to the register bank
      0x80010000        // Offset to the register bank (Enclustra board)
  );

  if( reg_map == MAP_FAILED ) {
    perror( "### mmap(GPIO)" );
    close( mem_fd );
    exit( -1 );
  }

  _regs = (volatile uint32_t *) reg_map;

  // LUT registers mmap
  reg_map = mmap(
      NULL,             // Any adddress in our space will do
      4096*4,           // Map length (4096 32-bit integers)
      PROT_READ|PROT_WRITE,// Enable reading & writing to mapped memory
      MAP_SHARED,       // Shared with other processes
      mem_fd,           // File-to-memory file descriptor
      //0x43B00000      // Offset to the register bank
      0x80000000        // Offset to the register bank (Enclustra board)
  );

  if( reg_map == MAP_FAILED ) {
    perror( "### mmap(LUT)" );
    close( mem_fd );
    exit( -1 );
  }

  _lut = (volatile uint32_t *) reg_map;

  // LTI registers mmap
  reg_map = mmap(
      NULL,             // Any adddress in our space will do
      1024,             // Map length
      PROT_READ|PROT_WRITE,// Enable reading & writing to mapped memory
      MAP_SHARED,       // Shared with other processes
      mem_fd,           // File-to-memory file descriptor
      0x80020000        // Offset to the register bank (Enclustra board)
  );

  if( reg_map == MAP_FAILED ) {
    perror( "### mmap(LTI)" );
    close( mem_fd );
    exit( -1 );
  }

  _lti = (volatile uint32_t *) reg_map;

  close( mem_fd );

  // DEBUG: set register contents
  //for( int i=0; i<REG_COUNT; ++i )
  //  _regs[i] = 0xAA0+i;

  // DEBUG: display register contents
  //_log << std::hex << std::setfill('0');
  //for( int i=0; i<REG_COUNT; ++i )
  //  _log << "Reg " << i << ": " << std::setw(8) << _regs[i]
  //       << "  " << REG_NAME[i] << std::endl;
  //_log << std::dec << std::setfill(' ');
}

// ----------------------------------------------------------------------------

RegsMem::~RegsMem()
{
  // DEBUG: display register contents
  _log << std::hex << std::setfill('0');
  for( int i=0; i<REG_COUNT; ++i )
    _log << "Reg " << i << " (" << std::setw(2) << (i*4) << "): "
         << std::setw(8) << _regs[i] << "  " << REG_NAME[i] << std::endl;
  _log << std::dec << std::setfill(' ');
}

// ----------------------------------------------------------------------------

int RegsMem::getVal( int index ) const
{
  if( _regs == 0 ) return 0;
  return _regs[index];
}

// ----------------------------------------------------------------------------

void RegsMem::setVal( int index, int value ) const
{
  if( _regs == 0 ) return;
  _regs[index] = value;

  // DEBUG: display register contents
  if( index == REG_ECR && value == 0 )
    {
      _log << std::hex << std::setfill('0');
      for( int i=0; i<REG_COUNT; ++i )
        _log << "Reg " << i << ": " << std::setw(8) << _regs[i]
             << "  " << REG_NAME[i] << std::endl;
      _log << std::dec << std::setfill(' ');
    }
}

// ----------------------------------------------------------------------------

void RegsMem::setLut( int index, uint32_t value ) const
{
  if( _lut == 0 ) return;
  _lut[index] = value;
}

// ----------------------------------------------------------------------------

uint32_t RegsMem::getLut( int index ) const
{
  if( _lut == 0 ) return 0;
  return _lut[index];
}

// ----------------------------------------------------------------------------

void RegsMem::resetGth()
{
  if( _lti == 0 ) return;
  _lti[0] = 0;
  usleep( 100000 ); // 100 ms
  _lti[0] = 1;
  usleep( 100000 ); // 100 ms
  _lti[0] = 0;
  usleep( 100000 ); // 100 ms
}

// ----------------------------------------------------------------------------
